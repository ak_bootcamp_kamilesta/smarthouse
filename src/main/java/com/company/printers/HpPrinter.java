package com.company.printers;

import com.company.smarthouse.Device;
import com.company.printerdriver.Hp;

public class HpPrinter implements Printer, Device {

    private Hp hpPrinter;

    public HpPrinter() {
        this.hpPrinter = new Hp();
    }

    @Override
    public void print(String textToPrint) {
        hpPrinter.print(textToPrint);
    }

    @Override
    public void turnOff() {
        System.out.println("Hp printer was turned off.");
    }
}
