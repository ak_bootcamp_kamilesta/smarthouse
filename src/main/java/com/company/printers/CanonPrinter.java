package com.company.printers;

import com.company.smarthouse.Device;
import com.company.printerdriver.Canon;

public class CanonPrinter implements Printer, Device {

    private Canon driver;

    public CanonPrinter() {
        driver = new Canon();
    }

    @Override
    public void print(String textToPrint) {
        driver.print("Canon print");
    }

    @Override
    public void turnOff() {
        System.out.println("Canon printer was turned off.");
    }

}
