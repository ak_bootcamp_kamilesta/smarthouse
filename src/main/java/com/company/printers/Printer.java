package com.company.printers;

public interface Printer {

    void print(String textToPrint);
}
