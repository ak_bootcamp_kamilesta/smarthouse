package com.company.printers;

import com.company.smarthouse.Device;
import com.company.printerdriver.Lg;

public class LgPrinter implements Printer, Device {

    private Lg lgPrinter;

    public LgPrinter() {
        this.lgPrinter = new Lg();
    }

    @Override
    public void print(String textToPrint) {
        lgPrinter.print(textToPrint);
    }

    @Override
    public void turnOff() {
        System.out.println("Lg printer is off.");
    }

}
