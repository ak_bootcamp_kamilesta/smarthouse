package com.company.tvs;

import com.company.smarthouse.Device;

public class PhilipsTv implements TV, Device {
    @Override
    public void changeChannel(int channelNumber) {
        System.out.println("You want to change channel of Phillips tv to " + channelNumber);
    }

    @Override
    public void turnOff() {
        System.out.println("Phillips tv is off.");
    }

}
