package com.company.tvs;

import com.company.smarthouse.Device;

public class SamsungTv implements TV, Device {


    @Override
    public void changeChannel(int channelNumber) {
        System.out.println("You changed your samsung to channel " + channelNumber);
    }

    @Override
    public void turnOff() {
        System.out.println("Samsung Tv is off.");
    }
}
