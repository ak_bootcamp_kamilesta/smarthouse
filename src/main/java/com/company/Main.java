package com.company;

import com.company.printers.HpPrinter;
import com.company.smarthouse.Device;
import com.company.smarthouse.SmartHouse;
import com.company.tvs.PhilipsTv;
import com.company.tvs.SamsungTv;

public class Main {

    public static void main(String[] args) {

        SmartHouse smartHouse = new SmartHouse();

        Device printer = new HpPrinter();
        Device samsungTv = new SamsungTv();
        Device philipsTv = new PhilipsTv();


        smartHouse.add(printer);
        smartHouse.add(samsungTv);
        smartHouse.add(philipsTv);

        smartHouse.turnOffAll();
    }
}
