package com.company.smarthouse;

import java.util.HashSet;
import java.util.Set;

public class SmartHouse {

    private Set<Device> devices;

    public SmartHouse() {
        this.devices = new HashSet<>();
    }

    public void add(Device device){
        this.devices.add(device);
    }

    public void turnOffAll(){
        this.devices.forEach(Device::turnOff);
        System.out.println("All devices turned off.");
    }
}
